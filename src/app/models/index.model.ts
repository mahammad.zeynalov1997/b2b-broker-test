export class Child {
  constructor(public id: string, public color: string) {}
}

export class DataElement {
  constructor(
    public id: string,
    public int: number,
    public float: number,
    public color: string,
    public child: Child
  ) {
    this.float = parseFloat(float.toFixed(18));
  }
}

export enum MessageType {
  SET_INTERVAL,
  SET_ARRAY_SIZE,
}

export const DEFAULTS = {
  TIME_INTERVAL: 2000,
  DATA_ARRAY_SIZE: 100,
  ADDITONAL_IDS: '12, 58, 23',
};
