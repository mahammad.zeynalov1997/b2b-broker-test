import { TestBed } from '@angular/core/testing';
import { DataService } from './data.service';
import { DataElement } from '../models/index.model';

export class Child {
  constructor(public id: string, public color: string) {}
}

describe('DataService', () => {
  let service: DataService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DataService],
    });
    service = TestBed.inject(DataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should update data', () => {
    const testData: DataElement[] = [
      new DataElement('1', 10, 3.14, 'red', new Child('1', 'blue')),
      new DataElement('2', 20, 6.28, 'blue', new Child('2', 'red')),
    ];

    service.updateData(testData);
    service.dataStream$.subscribe((data) => {
      expect(data).toEqual(testData);
    });
  });

  it('should trigger additional ids', () => {
    const testData: DataElement[] = [
      new DataElement('1', 10, 3.14, 'red', new Child('1', 'blue')),
      new DataElement('2', 20, 6.28, 'blue', new Child('2', 'red')),
    ];
    const additionalIds = '1, 2, 3';
    const expectedData: DataElement[] = [];

    service.updateData(testData);
    service.additionalIdsTrigger(additionalIds);

    service.dataStream$.subscribe((data) => {
      expect(data).toEqual(expectedData);
    });
  });

  it('should return original data if additional ids are not found', () => {
    const testData: DataElement[] = [
      new DataElement('1', 10, 3.14, 'red', new Child('1', 'blue')),
      new DataElement('2', 20, 6.28, 'blue', new Child('2', 'red')),
    ];
    const additionalIds = '3, 4, 5';

    service.updateData(testData);
    service.additionalIdsTrigger(additionalIds);

    service.dataStream$.subscribe((data) => {
      expect(data).toEqual(testData);
    });
  });
});
