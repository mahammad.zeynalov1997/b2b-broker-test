import { Injectable } from '@angular/core';
import { DataService } from './data.service';
import { MessageType } from '../models/index.model';

@Injectable({
  providedIn: 'root',
})
export class WebWorkerService {
  private worker!: Worker;

  constructor(private dataService: DataService) {
    this.startWorker();
  }

  private startWorker() {
    if (typeof Worker !== 'undefined') {
      this.worker = new Worker(
        new URL('./data-listener.worker', import.meta.url)
      );
      this.worker.onmessage = ({ data }) => {
        this.dataService.updateData(data);
      };
    }
  }

  public setTimerInterval(interval: number): void {
    this.worker.postMessage({ type: MessageType.SET_INTERVAL, interval });
  }

  public setDataArraySize(size: number): void {
    this.worker.postMessage({ type: MessageType.SET_ARRAY_SIZE, size });
  }
}
