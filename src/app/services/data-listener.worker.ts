/// <reference lib="webworker" />

import { MessageType } from '../models/index.model';
import { WorkerHelper } from '../utils/worker-helper';

const workerHelper = new WorkerHelper();

addEventListener('message', ({ data }) => {
  switch (data.type) {
    case MessageType.SET_INTERVAL:
      workerHelper.stopSocket();
      workerHelper.startSocket(data.interval);
      break;
    case MessageType.SET_ARRAY_SIZE:
      workerHelper.dataArraySize = data.size;
      break;
  }
});
