import { Injectable } from '@angular/core';
import { Subject, Observable, combineLatest, map } from 'rxjs';
import { DataElement } from '../models/index.model';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  private dataSubject: Subject<DataElement[]> = new Subject<DataElement[]>();
  private data$: Observable<DataElement[]> = this.dataSubject.asObservable();
  private additionalIdsSubject$ = new Subject<string[]>();
  public dataStream$ = combineLatest([
    this.data$,
    this.additionalIdsSubject$,
  ]).pipe(map(([data, input]) => this.shuffleArray(data, input).slice(0, 10)));

  constructor() {}

  public updateData(data: DataElement[]): void {
    this.dataSubject.next(data);
  }

  additionalIdsTrigger(input: string) {
    this.additionalIdsSubject$.next(input.split(',').map((i) => i.trim()));
  }

  private shuffleArray(data: DataElement[], input: string[]) {
    if (!input.length) {
      return data;
    } else {
      const foundDataElements: DataElement[] = [];
      input.forEach((id) => {
        const index = data.findIndex((i) => i.id === id);
        if (index !== -1) {
          const deletedElements = data.splice(index, 1);
          foundDataElements.push(deletedElements[0]);
        }
      });
      return foundDataElements.concat(data);
    }
  }
}
