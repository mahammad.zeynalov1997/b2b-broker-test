import { Component, inject } from '@angular/core';
import { DataService } from '../services/data.service';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-data-grid',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './data-grid.component.html',
})
export class DataGridComponent {
  private dataService = inject(DataService);
  data$ = this.dataService.dataStream$;
}
