import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataGridComponent } from './data-grid/data-grid.component';
import { WebWorkerService } from './services/web-worker.service';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { debounceTime } from 'rxjs';
import { DataService } from './services/data.service';
import { DEFAULTS } from './models/index.model';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, DataGridComponent, FormsModule, ReactiveFormsModule],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  timeIntervalControl = new FormControl<number>(DEFAULTS.TIME_INTERVAL);
  dataArraySizeControl = new FormControl<number>(DEFAULTS.DATA_ARRAY_SIZE);
  additionalIdsControl = new FormControl<string>(DEFAULTS.ADDITONAL_IDS);

  constructor(
    private webWorkerService: WebWorkerService,
    private dataService: DataService
  ) {}

  ngOnInit(): void {
    this.webWorkerService.setTimerInterval(DEFAULTS.TIME_INTERVAL);
    this.webWorkerService.setDataArraySize(DEFAULTS.DATA_ARRAY_SIZE);

    this.timeIntervalControl.valueChanges
      .pipe(debounceTime(400))
      .subscribe((value) => {
        if (typeof value === 'number') {
          this.webWorkerService.setTimerInterval(value);
        }
      });
    this.dataArraySizeControl.valueChanges
      .pipe(debounceTime(400))
      .subscribe((value) => {
        if (typeof value === 'number') {
          this.webWorkerService.setDataArraySize(value);
        }
      });

    this.additionalIdsControl.valueChanges
      .pipe(debounceTime(400))
      .subscribe((ids) => {
        if (ids) {
          this.dataService.additionalIdsTrigger(ids);
        }
      });
    this.additionalIdsControl.setValue(DEFAULTS.ADDITONAL_IDS);
  }
}
