import { Child, DEFAULTS, DataElement } from '../models/index.model';

export class WorkerHelper {
  intervalId: any;
  dataArraySize = DEFAULTS.DATA_ARRAY_SIZE;

  constructor() {}

  generateRandomData = (): DataElement[] => {
    const data = [];
    for (let i = 0; i < this.dataArraySize; i++) {
      const id = `${i}`;
      const int = Math.floor(Math.random() * 100);
      const float = Math.random() * 100;
      const color = this.generateRandomColor();
      const child = new Child(`${i}`, this.generateRandomColor());
      data.push(new DataElement(id, int, float, color, child));
    }
    return data;
  };

  sendData = () => {
    const data = this.generateRandomData();
    postMessage(data);
  };

  startSocket = (interval: number) => {
    this.intervalId = setInterval(this.sendData, interval);
  };

  stopSocket = () => {
    clearInterval(this.intervalId);
  };

  generateRandomColor = () => {
    const r = Math.floor(Math.random() * 256);
    const g = Math.floor(Math.random() * 256);
    const b = Math.floor(Math.random() * 256);
    return `rgb(${r}, ${g}, ${b})`;
  };
}
